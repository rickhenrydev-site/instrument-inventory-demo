/* eslint-disable no-undef */
const shared = {
  stage: 'demo',
  apiGateway: {
    REGION: 'us-east-1',
    URL: ' https://0sayiu8gd1.execute-api.us-east-1.amazonaws.com/demo/',
  }
  , s3: {
    REGION: 'us-east-1',
    BUCKET: 'instrument-inventory-demo-photosbucket-2rjcbxyhu9so',
  },
}

const cognito = {
  REGION: 'us-east-1',
  USER_POOL_ID: 'us-east-1_mdlcscRdA',
  APP_CLIENT_ID: '220hvavn2s21c6gcgfg7mvt080',
  IDENTITY_POOL_ID: 'us-east-1:2bc7b531-0d39-4489-ad87-0f7a4ea33eff',
}

const dev = {
  ...shared,
  cognito: {
    ...cognito,
    DOMAIN:
      process.env.USER === 'rick'
        ? 'localhost'
        : process.env.DEPLOY_URL.replace('https://', ''),
    SECURE: process.env.USER === 'rick' ? false : true,
  },
}

const prod = {
  ...shared,
  cognito: {
    ...cognito,
    DOMAIN: process.env.URL ? process.env.URL.replace('https://', '') : '',
    SECURE: true,
  },
}

const config = process.env.REACT_APP_STAGE === 'prod' ? prod : dev

export default config
