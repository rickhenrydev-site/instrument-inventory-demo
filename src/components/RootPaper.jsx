import { styled } from '@material-ui/styles'
import { Paper } from '@material-ui/core'

import { root } from '../globalStyles'

export default styled(Paper)({
  ...root,
})
