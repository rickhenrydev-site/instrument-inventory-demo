export const root = {
  padding: '1rem 1rem 0 1rem',
  margin: '1rem auto',
  maxWidth: 700,
  overflowX: 'auto',
}

export const lastButton = {
  marginLeft: 'auto',
}

export const fullWidth = {
  width: '100%',
}

export const centerStuff = {
  width: '100%',
  display: 'flex',
  justifyContent: 'space-around',
}
